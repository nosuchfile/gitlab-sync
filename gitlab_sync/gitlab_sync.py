#!/usr/bin/env python

import os
import sys
import subprocess
import urllib2
import ssl
import json
import yaml

def main():
    if len(sys.argv) > 1:
        config_file = sys.argv[1]
    else:
        config_file = os.path.join(os.environ['HOME'], ".gitlab-sync.yaml")

    with open(config_file) as fp:
        global_config = yaml.load(fp.read())

    def project_list(config):
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        pl = []

        page = 1
        per_page = 100

        while True:
            args = "&per_page=%d&page=%d" % (per_page, page)
            r = urllib2.urlopen("%(server)s/api/v4/projects/?private_token=%(token)s" % (config) + args, context=ctx)

            content = r.read()

            projects = json.loads(content)
            for p in projects:
                if 'owner' in p:
                    o = p['owner']
                    ns = o['username']
                else:
                    n = p['namespace']
                    ns = n['path']
                if not ns in config['namespaces']:
                    continue

                pl.append((p['path_with_namespace'], p['ssh_url_to_repo']))

            if len(projects) < per_page:
                break
            page += 1

        return pl

    ret = 0

    for server in global_config:
        config = global_config[server]
        pl = project_list(config)
        for path, git_url in pl:
            rel_path = os.path.join(config['repositories'], path)

            if not rel_path.endswith(".git"):
                rel_path += ".git"

            parent_path = os.path.dirname(rel_path)
            if not os.path.isdir(parent_path):
                os.makedirs(parent_path)

            if not os.path.exists(os.path.join(rel_path, "config")):
                p = subprocess.Popen("cd %s; git clone --bare %s" % (parent_path, git_url),
                        shell=True)
                stdout, stderr = p.communicate()
                if p.returncode != 0:
                    print("Error while cloning `%s': %s" % (git_url, stderr.rstrip()))
                    ret = 1
                    continue
                p = subprocess.Popen("cd %s; git config remote.origin.fetch '+refs/heads/*:refs/heads/*'" % (rel_path), shell=True)
                stdout, stderr = p.communicate()
                if p.returncode != 0:
                    print("Error while configuring `%s': %s" % (git_url, stderr.rstrip()))
                    ret = 1
                    continue
            else:
                p = subprocess.Popen("cd %s; git fetch origin" % (rel_path), shell=True)
                stdout, stderr = p.communicate()
                if p.returncode != 0:
                    print("Error while fetching`%s': %s" % (git_url, stderr.rstrip()))
                    ret = 1
                    continue

    return ret

if __name__ == "__main__":
    sys.exit(main())
