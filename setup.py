#!/usr/bin/env python

from setuptools import setup

setup(
    name="gitlab-sync",
    version='0.1',
    author="Gregory Thiemonge",
    author_email="greg@thiemonge.org",
    description="Synchronize remote gitlab repositories",
    license="Proprietary",
    keywords="gitlab, git, synchronization",
    classifiers=[
        "License :: Other/Proprietary License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: Utilities"],
    packages=['gitlab_sync'],
    entry_points={
        'console_scripts': [
            'gitlab-sync=gitlab_sync.gitlab_sync:main',
        ]
    },
)
